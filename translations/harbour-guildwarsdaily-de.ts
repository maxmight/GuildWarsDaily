<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccessTokenDialog</name>
    <message>
        <source>Access Token</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Account</name>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Guild Wars
Daily</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CurrencyExchange</name>
    <message>
        <source>Currency Exchange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Convert gems to coins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Convert coins to gems</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connection Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please check your internet connection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CurrencyExchangeBlock</name>
    <message>
        <source>You can exchange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>gems for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>gold coins for</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DailyAchievements</name>
    <message>
        <source>Daily Achievements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventTimers</name>
    <message>
        <source>Event Timers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HomePage</name>
    <message>
        <source>Guild Wars Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Knowledge is the greatest treasure.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Daily Achievements</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ready for the next one.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>World vs. World</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>We all must do our part.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Event Timers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There&apos;s something in the water. Help!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Currency Exchange</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Looking to spend some coin?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Storylines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>History never lies. Historians however...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>AhoooOOoooh!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Access Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete Access Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deleting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetworkResourcePage</name>
    <message>
        <source>Connection Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please check your internet connection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>News</name>
    <message>
        <source>News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Error Loading RSS Feed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewsItem</name>
    <message>
        <source>News</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>by</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Story</name>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Storylines</name>
    <message>
        <source>Storylines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>stories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>story</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewAchievement</name>
    <message>
        <source>Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Required Access</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WvW</name>
    <message>
        <source>Select a world</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connection Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please check your internet connection</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WvWServerPanel</name>
    <message>
        <source>deaths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>score</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kills</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Camps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Towers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Keeps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Castle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
