# Guild Wars Daily

A Sailfish OS client for the Guild Wars 2 API.

## Features

Currently the application supports the following features:

* News
* Daily Achievements
* World vs. World
* Event Timers
* Storylines
* Currency Exchange
