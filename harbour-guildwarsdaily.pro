# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

include(vendor/duperagent/com_cutehacks_duperagent.pri)

# The name of your application
TARGET = harbour-guildwarsdaily

CONFIG += sailfishapp

SOURCES += src/GuildWarsDaily.cpp

OTHER_FILES += \
    qml/cover/CoverPage.qml \
    rpm/harbour-guildwarsdaily.changes.in \
    rpm/harbour-guildwarsdaily.spec \
    rpm/harbour-guildwarsdaily.yaml \
    translations/*.ts \
    harbour-guildwarsdaily.desktop

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172 256x256

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
# TRANSLATIONS += translations/harbour-guildwarsdaily-de.ts

DISTFILES += \
    qml/pages/account/MasteryPoints.qml

# Note: version number is configured in yaml
DEFINES += APP_VERSION=\\\"$$VERSION\\\" APP_NAME=\\\"$$TARGET\\\"
