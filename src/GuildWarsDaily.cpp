#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <memory>

#include <QGuiApplication>
#include <QQuickView>

#include <sailfishapp.h>

using std::unique_ptr;

int main(int argc, char *argv[])
{
    // SailfishApp::main() will display "qml/template.qml", if you need more
    // control over initialization, you can use:
    //
    //   - SailfishApp::application(int, char *[]) to get the QGuiApplication *
    //   - SailfishApp::createView() to get a new QQuickView * instance
    //   - SailfishApp::pathTo(QString) to get a QUrl to a resource file
    //
    // To display the view, call "show()" (will show fullscreen on device).

    unique_ptr<QGuiApplication> app(SailfishApp::application(argc, argv));
    app->setApplicationName(QStringLiteral(APP_NAME));
    app->setApplicationVersion(QStringLiteral(APP_VERSION));

    unique_ptr<QQuickView> view(SailfishApp::createView());
    view->setSource(SailfishApp::pathToMainQml());
    view->show();

    return app->exec();
}

