import QtQuick 2.2
import Sailfish.Silica 1.0

import "../components/" as Components
import "../js/server.js" as Service
import "../js/currency.js" as Currency

Page {
    SilicaFlickable {
        id: flickable
        state: "GEMSTOCOINS"
        anchors.fill: parent

        anchors {
            fill: parent
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Convert gems to coins")
                onClicked: {
                    contentItem.state = "INITIAL";
                    flickable.state = "GEMSTOCOINS";
                }
            }

            MenuItem {
                text: qsTr("Convert coins to gems")
                onClicked: {
                    contentItem.state = "INITIAL";
                    flickable.state = "COINSTOGEMS";
                }
            }
        }

        Column {
            id: column
            width: parent.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("Currency Exchange")
            }

            Row {
                width: parent.width - Theme.horizontalPageMargin
                height: textField.height
                anchors.left: parent.left
                anchors.leftMargin: Theme.horizontalPageMargin

                Image {
                    id: currencyIcon
                    width: textField.height * 0.6
                    height: width
                    anchors.verticalCenter: parent.verticalCenter

                    fillMode: Image.PreserveAspectFit
                }

                TextField {
                    id: textField
                    width: parent.width - currencyIcon.width
                    inputMethodHints: Qt.ImhDigitsOnly
                    labelVisible: false
                    horizontalAlignment: TextInput.AlignRight

                    EnterKey.enabled: text.length > 1
                    EnterKey.onClicked: {
                        convert();
                        textField.focus = false;
                    }
                }
            }
        }

        Item {
            id: contentItem
            anchors {
                left: parent.left
                right: parent.right
                top: column.bottom
                bottom: parent.bottom
            }

            state: "INITIAL"

            Components.CurrencyExchangePanel {
                id: resultPanel
                width: parent.width
                anchors.centerIn: parent
                state: flickable.state
            }

            Components.TextPlaceholder {
                id: viewPlaceholder
                width: parent.width - (2 * Theme.horizontalPageMargin)
                anchors.centerIn: parent
                visible: false
                text: qsTr("Connection Error")
                hintText: qsTr("Please check your internet connection")
            }

            BusyIndicator {
                id: busyIndicator
                size: BusyIndicatorSize.Large
                anchors.centerIn: parent
                running: false
            }

            states: [
                State {
                    name: "INITIAL"
                    PropertyChanges {
                        target: resultPanel
                        visible: false
                    }
                    PropertyChanges {
                        target: busyIndicator
                        running: false
                    }
                    PropertyChanges {
                        target: viewPlaceholder
                        visible: false
                    }
                },
                State {
                    name: "RUNNING"
                    PropertyChanges {
                        target: busyIndicator
                        running: true
                    }
                    PropertyChanges {
                        target: resultPanel
                        visible: false
                    }
                },
                State {
                    name: "SHOWCONTENT"
                    PropertyChanges {
                        target: busyIndicator
                        running: false
                    }
                    PropertyChanges {
                        target: resultPanel
                        visible: true
                    }
                },
                State {
                    name: "NETWORKERROR"
                    PropertyChanges {
                        target: resultPanel
                        visible: false
                    }
                    PropertyChanges {
                        target: busyIndicator
                        running: false
                    }
                    PropertyChanges {
                        target: viewPlaceholder
                        visible: true
                    }
                }
            ]
        }

        states: [
            State {
                name: "GEMSTOCOINS"
                PropertyChanges {
                    target: currencyIcon
                    source: "../images/ui_gem.svg"
                }
            },
            State {
                name: "COINSTOGEMS"
                PropertyChanges {
                    target: currencyIcon
                    source: "../images/ui_coin_gold.svg"
                }
            }
        ]
    }

    function convert() {
        contentItem.state = "RUNNING";
        var value = textField.text;

        if (flickable.state == "GEMSTOCOINS") {
            Service.exchangeGems(value, function(gemsResponse) {
                var sellPriceCoins = Currency.quantityToCoins(gemsResponse.quantity);

                resultPanel.quantity = value;
                resultPanel.goldCoinsValue = sellPriceCoins.gold;
                resultPanel.silverCoinsValue = sellPriceCoins.silver;
                resultPanel.copperCoinsValue = sellPriceCoins.copper;

                contentItem.state = "SHOWCONTENT";
            }, showError);
        } else {
            Service.exchangeCoins(value * 100 * 100, function(coinsResponse) {
                resultPanel.quantity = value;
                resultPanel.gemsValue = coinsResponse.quantity;

                contentItem.state = "SHOWCONTENT";
            }, showError);
        }
    }

    function showError(statusCode, statusText) {
        if (statusCode !== 0 && statusText.length > 0) {
            viewPlaceholder.hintText = statusCode + " " + statusText
        }

        contentItem.state = "NETWORKERROR"
    }
}
