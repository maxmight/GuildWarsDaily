import QtQuick 2.2
import Sailfish.Silica 1.0

import "../components/" as Components
import "../js/server.js" as Service

Components.NetworkResourcePage {
    property var storylines: []

    id: root

    SilicaListView {
        id: listView
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Storylines")
        }

        model: ListModel {
            id: listModel
        }

        PullDownMenu {
            quickSelect: true

            MenuItem {
                text: qsTr("Refresh")
                onClicked: listView.loadContent()
            }
        }

        delegate: ListItem {
            id: listItem

            width: parent.width
            contentHeight: nameLabel.height + descriptionLabel.height + (2 * Theme.paddingMedium)

            anchors {
                left: parent.left
                right: parent.right
            }

            onClicked: {
                pageStack.push(
                            Qt.resolvedUrl("Story.qml"),
                            {
                                "chapterName": listModel.get(index).name,
                                "stories": storylines[index].stories
                            });
            }

            Label {
                id: nameLabel

                y: Theme.paddingMedium
                width: parent.width

                text: model.name
                font.pixelSize: Theme.fontSizeMedium
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: listItem.highlighted ? Theme.highlightColor : Theme.primaryColor

                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
            }

            Label {
                id: descriptionLabel

                height: text.length > 0 ? contentHeight : 0

                text: model.numberOfStories + " " + (model.numberOfStories === 1 ? qsTr("story") : qsTr("stories"))
                font.pixelSize: Theme.fontSizeSmall
                color: listItem.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere

                anchors {
                    top: nameLabel.bottom
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                    bottomMargin: Theme.paddingMedium
                }
            }
        }

        Component.onCompleted: {
            loadContent();
        }

        function loadContent() {
            listModel.clear();
            root.state = stateLoading;

            Service.getStorySeasons(function(seasons) {
                root.state = stateNormal;

                seasons.sort(function(a, b) {
                    return a.order - b.order;
                });

                seasons.forEach(function(season, index) {
                    listModel.append({name: season.name, numberOfStories: season.stories.length});
                });

                storylines = seasons;
            }, function(statusCode, statusText) {
                setErrorMessage(statusCode, statusText);
                root.state = stateNetworkError;
            });
        }
    }
}
