import QtQuick 2.2
import Sailfish.Silica 1.0

Page {
    id: viewAchievementPage

    property var achievement;

    PageHeader {
        id: header
        title: achievement.name
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
    }

    Column {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            leftMargin: Theme.horizontalPageMargin
            rightMargin: Theme.horizontalPageMargin
        }

        spacing: Theme.paddingLarge

        Label {
            id: descriptionLabel

            width: parent.width
            visible: text.length > 0

            color: Theme.secondaryHighlightColor
            text: achievement.description
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        Label {
            id: requirementLabel

            width: parent.width
            visible: text.length > 0

            font.pixelSize: Theme.fontSizeSmall
            text: achievement.requirement
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere

            anchors {
                leftMargin: Theme.horizontalPageMargin
                rightMargin: Theme.horizontalPageMargin
            }
        }

        DetailItem {
            label: qsTr("Level")
            value: {
                if (achievement.level.min !== achievement.level.max) {
                    return achievement.level.min + " - " + achievement.level.max;
                }

                return achievement.level.min;
            }
        }

        DetailItem {
            label: qsTr("Required Access")
            value: {
                var names = {
                    "GuildWars2": "Guild Wars 2",
                    "HeartOfThorns": "Heart of Thorns",
                    "PathOfFire": "Path of Fire",
                    "EndOfDragons": "End of Dragons",
                    "SecretsOfTheObscure": "Secrets of the Obscure",
                    "JanthirWilds": "Janthir Wilds",
                };

                var result = [];
                for (var i = 0; i < achievement.requiredAccess.count; i++) {
                    var item = achievement.requiredAccess.get(i).name;
                    result.push(item);
                }

                result = result.map(function(value) {
                    return names[value];
                });

                return result.join('\n');
            }
        }
    }
}

