import QtQuick 2.2
import Sailfish.Silica 1.0
import Nemo.Configuration 1.0

import "../components/" as Components

import "../js/server.js" as Service

Page {
    id: page

    property var worlds: []

    ConfigurationGroup {
        id: wvwConfiguration
        path: "/apps/guildwarsdaily/wvw"

        property int selectedWorldId: -1
    }


    Drawer {
        id: drawer

        open: false
        anchors.fill: parent
        backgroundSize: parent.height * 0.8

        background: SilicaListView {
            anchors.fill: parent

            header: SearchField {
                width: parent.width
                placeholderText: qsTr("Select a world")

                onTextChanged: {
                    worldsListModel.update(text);
                }
            }

            // prevent newly added list delegates from stealing focus away from the search field
            currentIndex: -1

            model: ListModel {
                id: worldsListModel

                function update(text) {
                    clear()
                    text = text.toLowerCase();

                    for (var i = 0; i < worlds.length; i++) {
                        if (text === "" || worlds[i].name.toLowerCase().indexOf(text) >= 0) {
                            append(worlds[i]);
                        }
                    }
                }
            }

            delegate: ListItem {
                height: Theme.itemSizeSmall

                Label {
                    text: model.name

                    anchors {
                        left: parent.left
                        leftMargin: Theme.paddingLarge
                        verticalCenter: parent.verticalCenter
                    }
                }

                onClicked: {
                    pageHeader.title = model.name;
                    wvwConfiguration.selectedWorldId = model.id;
                    drawer.open = false;
                    worldsListModel.update("");
                    loadContent(wvwConfiguration.selectedWorldId);
                }
            }

            VerticalScrollDecorator {}
        }

        SilicaFlickable {
            anchors.fill: parent
            contentWidth: parent.width
            contentHeight: column.height
            state: "NORMAL"

            PullDownMenu {
                MenuItem {
                    text: qsTr("Select a world")
                    onClicked: drawer.open = true
                }

                MenuItem {
                    text: qsTr("Refresh")
                    onClicked: loadContent(wvwConfiguration.selectedWorldId)
                }
            }

            Column {
                id: column
                width: parent.width
                spacing: Theme.paddingMedium * 2

                anchors {
                    leftMargin: Theme.paddingMedium
                    rightMargin: Theme.paddingMedium
                }

                PageHeader {
                    id: pageHeader
                    title: qsTr("Select a world")
                    wrapMode: Text.WrapAtWordBoundaryOrAnywhere

                    MouseArea {
                        anchors.fill: parent
                        onClicked: drawer.open = !drawer.open
                    }
                }

                Components.WvWServerPanel {
                    id: serverRedPanel
                }

                Components.WvWServerPanel {
                    id: serverBluePanel
                }

                Components.WvWServerPanel {
                    id: serverGreenPanel
                }
            }

            BusyIndicator {
                id: busyIndicator
                size: BusyIndicatorSize.Large
                anchors.centerIn: parent
                running: false
            }

            ViewPlaceholder {
                id: viewPlaceholder
                enabled: false
                text: qsTr("Connection Error")
                hintText: qsTr("Please check your internet connection")
            }

            VerticalScrollDecorator {}
        }

        states: [
            State {
                name: "NORMAL"
                PropertyChanges { target: column; visible: true }
                PropertyChanges { target: busyIndicator; running: false }
            },
            State {
                name: "RUNNING"
                PropertyChanges { target: column; visible: false }
                PropertyChanges { target: busyIndicator; running: true }
            },
            State {
                name: "SELECTAWORLD"
                when: (wvwConfiguration.selectedWorldId == -1)
                PropertyChanges { target: column; visible: false }
            },
            State {
                name: "NETWORKERROR"
                PropertyChanges { target: column; visible: false }
                PropertyChanges { target: busyIndicator; running: false }
                PropertyChanges { target: viewPlaceholder; enabled: true }
            }
        ]
    }

    Component.onCompleted: {
        Service.getWorlds("all")
            .then(function(worldsResponse) {
                worldsListModel.clear();
                worldsListModel.append(worldsResponse.body);
                worlds = worldsResponse.body;
            })
            .then(function() {
                if (wvwConfiguration.selectedWorldId == -1) {
                    drawer.open = true;
                } else {
                    loadContent(wvwConfiguration.selectedWorldId);
                }
            });
    }

    function loadContent(worldId) {
        drawer.state = "RUNNING";

        Service.getWorldMatches(worldId)
            .then(function(worldMatchesResponse) {
                var response = worldMatchesResponse.body;
                var objectives = countObjectives(response.maps);
                pageHeader.title = getWorldName(worldId)

                serverRedPanel.serverName = getWorldNames(response.all_worlds.red);
                serverRedPanel.serverKills = response.kills.red.toLocaleString();
                serverRedPanel.serverScore = response.scores.red.toLocaleString();
                serverRedPanel.serverDeaths = response.deaths.red.toLocaleString();
                serverRedPanel.serverCamps = objectives.Red.Camp;
                serverRedPanel.serverTowers = objectives.Red.Tower;
                serverRedPanel.serverKeeps = objectives.Red.Keep;
                serverRedPanel.serverCastle = objectives.Red.Castle;

                serverBluePanel.serverName = getWorldNames(response.all_worlds.blue);
                serverBluePanel.serverKills = response.kills.blue.toLocaleString();
                serverBluePanel.serverScore = response.scores.blue.toLocaleString();
                serverBluePanel.serverDeaths = response.deaths.blue.toLocaleString();
                serverBluePanel.serverCamps = objectives.Blue.Camp;
                serverBluePanel.serverTowers = objectives.Blue.Tower;
                serverBluePanel.serverKeeps = objectives.Blue.Keep;
                serverBluePanel.serverCastle = objectives.Blue.Castle;

                serverGreenPanel.serverName = getWorldNames(response.all_worlds.green);
                serverGreenPanel.serverKills = response.kills.green.toLocaleString();
                serverGreenPanel.serverScore = response.scores.green.toLocaleString();
                serverGreenPanel.serverDeaths = response.deaths.green.toLocaleString();
                serverGreenPanel.serverCamps = objectives.Green.Camp;
                serverGreenPanel.serverTowers = objectives.Green.Tower;
                serverGreenPanel.serverKeeps = objectives.Green.Keep;
                serverGreenPanel.serverCastle = objectives.Green.Castle;

                drawer.state = "NORMAL";
            }).catch(function(reason) {
                viewPlaceholder.hintText = reason.toString();
                drawer.state = "NETWORKERROR";
            });
    }

    function countObjectives(maps) {
        var result = {
            "Red": { "Camp": 0, "Tower": 0, "Keep": 0, "Castle": 0 },
            "Green": { "Camp": 0, "Tower": 0, "Keep": 0, "Castle": 0 },
            "Blue": { "Camp": 0, "Tower": 0, "Keep": 0, "Castle": 0 }
        };

        maps.forEach(function(map) {
            map.objectives.forEach(function(objective) {
                if (result[objective.owner] === undefined) {
                    return;
                }

                if (result[objective.owner][objective.type] === undefined) {
                    return;
                }

                result[objective.owner][objective.type]++;
            });
        });

        return result;
    }

    function getWorldName(id) {
        for (var i = 0; i < worldsListModel.count; i++) {
            var world = worldsListModel.get(i);

            if (world.id === id) {
                return world.name;
            }
        }

        return qsTr("Unknown");
    }

    function getWorldNames(ids) {
        var names = [];

        for (var i = 0; i < worldsListModel.count; i++) {
            var world = worldsListModel.get(i);

            if (ids.indexOf(world.id) >= 0) {
                names.push(world.name);
            }
        }

        return names.join("\n")
    }
}
