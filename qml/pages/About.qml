import QtQuick 2.2
import Sailfish.Silica 1.0

Page {
    property string websiteURL: "https://gitlab.com/maxmight/GuildWarsDaily"

    SilicaFlickable {
        anchors {
            fill: parent
        }

        contentHeight: column.height

        VerticalScrollDecorator {}

        Column {
            id: column
            width: parent.width
            spacing: Theme.paddingLarge

            PageHeader {
                title: qsTr("About")
            }

            Image {
                source: "../images/logo.svg"
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Label {
                width: parent.width
                text: qsTr("Guild Wars Daily for Sailfish OS.")
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: Theme.fontSizeSmall
                horizontalAlignment: Text.AlignHCenter
            }

            BackgroundItem {
                width: parent.width
                height: website.height + 2 * Theme.paddingMedium

                onClicked: Qt.openUrlExternally(websiteURL)

                Label {
                    id: website
                    text: websiteURL
                    color: Theme.highlightColor
                    font.pixelSize: Theme.fontSizeExtraSmall
                    anchors.centerIn: parent
                }
            }

            DetailItem {
                label: qsTr("License")
                value: "BSD 3-Clause"
            }

            DetailItem {
                label: qsTr("Version")
                value: Qt.application.version
            }

            Label {
                text: qsTr("Guild Wars Copyright and Trademark Information

©2010–2019 ArenaNet, LLC. All rights reserved. Guild Wars, Guild Wars 2, Heart of Thorns, Guild Wars 2: Path of Fire, ArenaNet, NCSOFT, the Interlocking NC Logo, and all associated logos and designs are trademarks or registered trademarks of NCSOFT Corporation. All other trademarks are the property of their respective owners.")
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                font.pixelSize: Theme.fontSizeExtraSmall

                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
            }
        }
    }
}

