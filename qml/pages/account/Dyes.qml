import QtQuick 2.2
import Sailfish.Silica 1.0

import "../../components/" as Components
import "../../js/server.js" as Service

Components.NetworkResourcePage {
    id: root

    SilicaGridView {
        id: grid
        anchors.fill: parent
        cellWidth: width / 4
        cellHeight: cellWidth

        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: loadContent()
            }
        }

        header: PageHeader {
            title: qsTr("Dyes")
        }

        model: ListModel {
            id: listModel
        }

        delegate: ListItem {
            width: grid.cellWidth
            height: grid.cellHeight
            contentHeight: height
            onClicked: {
                panel.setColor(Qt.rgba(model.red, model.green, model.blue, 1),
                               model.name, model.categories)

                if (!panel.open) {
                    panel.open = true;
                }
            }

            Rectangle {
                anchors.fill: parent
                color: Qt.rgba(model.red, model.green, model.blue, 1)
            }

            Label {
                text: model.name
                height: parent.height
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: {
                    left: Theme.paddingSmall
                    right: Theme.paddingSmall
                }

                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WordWrap
                elide: Text.ElideRight
            }
        }

        VerticalScrollDecorator {}
    }

    DockedPanel {
        id: panel

        width: parent.width
        height: Theme.itemSizeExtraLarge + Theme.paddingLarge
        dock: Dock.Bottom

        BackgroundItem {
            anchors.fill: parent
            onClicked: panel.open = false
            contentItem.color: Theme.rgba("#000000", 0.9)

            Rectangle {
                id: colorRectangle
                x: Theme.paddingLarge
                width: parent.height - (2 * Theme.paddingLarge)
                height: width
                anchors.verticalCenter: parent.verticalCenter
            }

            Label {
                id: nameLabel
                font.pixelSize: Theme.fontSizeMedium
                elide: Text.ElideRight
                color: Theme.primaryColor
                anchors {
                    left: colorRectangle.right
                    leftMargin: Theme.paddingMedium
                    right: parent.right
                    rightMargin: Theme.paddingMedium
                    bottom: parent.verticalCenter
                }
            }

            Label {
                id: categoriesLabel
                font.pixelSize: Theme.fontSizeSmall
                elide: Text.ElideRight
                color: Theme.secondaryColor
                anchors {
                    left: colorRectangle.right
                    leftMargin: Theme.paddingMedium
                    right: parent.right
                    rightMargin: Theme.paddingMedium
                    top: parent.verticalCenter
                }
            }
        }

        function setColor(color, name, categories) {
            colorRectangle.color = color;
            nameLabel.text = name;
            categoriesLabel.text = categories;
        }
    }

    Component.onCompleted: loadContent();

    function loadContent() {
        listModel.clear();
        root.state = stateLoading;

        Service.getAccountDyes(accountSettings.accessToken)
            .then(function(dyes) {
                dyes.forEach(function(color, index) {
                    listModel.append({
                                         name: color.name,
                                         red: color.leather.rgb[0] / 256,
                                         green: color.leather.rgb[1] / 256,
                                         blue: color.leather.rgb[2] / 256,
                                         categories: color.categories.join(', ')
                                     })
                });

                root.state = stateNormal;
            }).catch(function(reason) {
                setErrorText(reason.toString());
                root.state = stateNetworkError;
            });
    }
}
