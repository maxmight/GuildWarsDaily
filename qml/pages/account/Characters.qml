import QtQuick 2.2
import Sailfish.Silica 1.0

import "../../components/" as Components
import "../../js/server.js" as Service

Components.NetworkResourcePage {
    id: root

    SilicaListView {
        id: listView
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Characters")
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: loadContent()
            }
        }

        model: ListModel { id: listModel }

        delegate: Components.CommonListItem {
            titleText: model.name
            onClicked: {
                pageStack.push(
                            Qt.resolvedUrl("CharacterCore.qml"),
                            { "characterName": model.name });

            }
        }

        VerticalScrollDecorator {}
    }

    Component.onCompleted: loadContent();

    function loadContent() {
        listModel.clear();
        root.state = stateLoading;

        Service.getCharacters(accountSettings.accessToken)
            .then(function(characters) {
                characters.forEach(function(character, index) {
                    listModel.append({name: character});
                });

                root.state = stateNormal;
            }).catch(function(reason) {
                setErrorText(reason.toString());
                root.state = stateNetworkError;
            });
    }
}
