import QtQuick 2.2
import Sailfish.Silica 1.0

import "../../components/" as Components
import "../../js/server.js" as Service

Components.NetworkResourcePage {
    id: root

    SilicaListView {
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Mastery Points")
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: loadContent()
            }
        }

        model: ListModel { id: listModel }

        delegate: Components.CommonListItem {
            titleText: model.region
            descriptionText: {
                return qsTr("%1 Earned  %2 Spent")
                    .replace("%1", model.earned)
                    .replace("%2", model.spent);
            }
        }

        VerticalScrollDecorator {}
    }

    Component.onCompleted: loadContent();

    function loadContent() {
        listModel.clear();
        root.state = stateLoading;

        Service.getAccountMasteryPoints(accountSettings.accessToken)
            .then(function(masteryPointsResponse) {
                var masteryPoints = masteryPointsResponse.body;
                listModel.append(masteryPoints.totals);
                root.state = stateNormal;
            }).catch(function(reason) {
                root.setErrorText(reason.toString());
                root.state = stateNetworkError;
            });
    }
}
