import QtQuick 2.2
import Sailfish.Silica 1.0

import "../../components/" as Components
import "../../js/server.js" as Service
import "../../js/currency.js" as Currency

Components.NetworkResourcePage {
    id: root

    SilicaListView {
        id: listView
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Wallet")
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: loadContent()
            }
        }

        model: ListModel { id: listModel }

        delegate: ListItem {
            width: parent.width
            contentHeight: Theme.itemSizeExtraSmall
            onClicked: {
                panel.setCurrency(model.icon, model.name, model.description);

                if (!panel.open) {
                    panel.open = true;
                }
            }

            Label {
                id: currencyName
                text: model.name
                font.pixelSize: Theme.fontSizeSmall

                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }
            }

            Label {
                id: currencyValue
                text: model.valueString
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeExtraSmall
                visible: model.id !== 1

                anchors {
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }
            }

            Loader {
                anchors {
                    right: parent.right
                    rightMargin: Theme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }

                Component.onCompleted: {
                    if (model.coins === undefined) {
                        return;
                    }

                    setSource("../../components/CoinsPanel.qml", {
                                  goldCoinsValue: model.coins.gold,
                                  silverCoinsValue: model.coins.silver,
                                  copperCoinsValue: model.coins.copper
                              });
                }
            }
        }

        VerticalScrollDecorator {}
    }

    DockedPanel {
        id: panel

        width: parent.width
        height: backgroundItem.height
        dock: Dock.Bottom

        BackgroundItem {
            id: backgroundItem
            width: parent.width
            height: column.height + 2 * Theme.paddingLarge
            onClicked: panel.open = false
            contentItem.color: Theme.rgba("#000000", 0.9)

            Column {
                id: column
                width: parent.width
                anchors.verticalCenter: parent.verticalCenter
                spacing: Theme.paddingMedium

                Image {
                    id: currencyImage
                    anchors.horizontalCenter: parent.horizontalCenter
                }

                Label {
                    id: nameLabel
                    font.pixelSize: Theme.fontSizeMedium
                    elide: Text.ElideRight
                    horizontalAlignment: Text.AlignHCenter
                    color: Theme.primaryColor
                    anchors {
                        left: parent.left
                        leftMargin: Theme.horizontalPageMargin
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }
                }

                Label {
                    id: descriptionLabel
                    font.pixelSize: Theme.fontSizeSmall
                    wrapMode: Text.Wrap
                    color: Theme.secondaryColor
                    anchors {
                        left: parent.left
                        leftMargin: Theme.horizontalPageMargin
                        right: parent.right
                        rightMargin: Theme.horizontalPageMargin
                    }
                }
            }
        }

        function setCurrency(image, name, description) {
            currencyImage.source = image;
            nameLabel.text = name;
            descriptionLabel.text = description;
        }
    }

    Component.onCompleted: loadContent();

    function loadContent() {
        listModel.clear();
        root.state = stateLoading;

        Service.getAccountWallet(accountSettings.accessToken)
            .then(function(currencies) {
                currencies.forEach(function(currency) {
                    if (currency.id === 1) {
                        currency.coins = Currency.quantityToCoins(currency.value);
                    }

                    currency.valueString = Number(currency.value).toLocaleString();
                });

                currencies.sort(function(a, b) {
                    return a.order - b.order;
                });

                listModel.append(currencies);
                root.state = stateNormal;
            }).catch(function(reason) {
                setErrorText(reason.toString());
                root.state = stateNetworkError;
            });
    }
}
