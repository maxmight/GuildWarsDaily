import QtQuick 2.2
import Sailfish.Silica 1.0

import "../../components/" as Components
import "../../js/server.js" as Service

Components.NetworkResourcePage {
    id: root

    SilicaListView {
        header: PageHeader {
            title: qsTr("Achievements")
        }



        VerticalScrollDecorator {}
    }

    Component.onCompleted: loadContent();

    function loadContent() {

        Service.getAccountAchievements(accountSettings.accessToken)
            .then(function(achievements) {
                console.log(achievements);
            })
            .catch(function(reason) {
                console.error(reason);
            });
    }
}
