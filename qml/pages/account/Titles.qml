import QtQuick 2.2
import Sailfish.Silica 1.0

import "../../components/" as Components
import "../../js/server.js" as Service

Components.NetworkResourcePage {
    id: root

    SilicaListView {
        id: listView
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Titles")
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: loadContent()
            }
        }

        model: ListModel { id: listModel }

        delegate: Components.CommonListItem {
            titleText: model.name
        }

        VerticalScrollDecorator {}
    }

    Component.onCompleted: loadContent();

    function loadContent() {
        listModel.clear();
        root.state = stateLoading;

        Service.getAccountTitles(accountSettings.accessToken)
            .then(function(titlesResponse) {
                var titles = titlesResponse.body;

                titles.sort(function(a, b) {
                    return b.id - a.id;
                });

                titles.forEach(function(title, index) {
                    listModel.append({name: title.name});
                });

                root.state = stateNormal;
            }).catch(function(reason) {
                setErrorText(reason.toString());
                root.state = stateNetworkError;
            });
    }
}
