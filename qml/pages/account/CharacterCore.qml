import QtQuick 2.2
import Sailfish.Silica 1.0

import "../../components/" as Components
import "../../js/server.js" as Service

Components.NetworkResourcePage {
    id: root

    property string characterName

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            quickSelect: true

            MenuItem {
                text: qsTr("Refresh")
                onClicked: loadContent()
            }
        }

        PageHeader {
            id: header
            title: characterName
        }

        Column {
            width: parent.width - (2 * Theme.horizontalPageMargin)
            anchors.top: header.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            visible: root.state == stateNormal

            Label {
                id: professionLabel
                anchors.left: parent.left
                color: Theme.primaryColor
                font.pixelSize: Theme.fontSizeMedium
            }

            Label {
                id: raceLabel
                anchors.left: parent.left
                color: Theme.secondaryColor
                font.pixelSize: Theme.fontSizeSmall
            }

            DetailItem { id: levelDetailItem; label: qsTr("Level") }

            DetailItem { id: guildDetailItem; label: qsTr("Guild") }

            DetailItem { id: deathsDetailItem; label: qsTr("Deaths") }

            DetailItem { id: playtimeDetailItem; label: qsTr("Playtime") }

            VerticalScrollDecorator {}
        }
    }

    Component.onCompleted: loadContent()

    function loadContent() {
        root.state = stateLoading;

        Service.getCharacterCore(accountSettings.accessToken, characterName)
            .then(function(characterResponse) {
                characterResponse = characterResponse.body;
                professionLabel.text = characterResponse.profession;
                raceLabel.text = characterResponse.race;
                levelDetailItem.value = characterResponse.level;
                deathsDetailItem.value = characterResponse.deaths;
                playtimeDetailItem.value =
                        Math.floor(characterResponse.age / (60 * 60)) + " " + qsTr("hours");

                if (characterResponse.hasOwnProperty("title")) {
                    Service.getTitle(characterResponse.title)
                        .then(function(titleResponse) {
                            header.description = titleResponse.body.name;
                        });
                }

                if (characterResponse.hasOwnProperty("guild") &&
                        characterResponse.guild !== null) {
                    Service.getGuild(characterResponse.guild)
                        .then(function(guildResponse) {
                            guildDetailItem.value = guildResponse.body.name;
                            guildDetailItem.visible = true;
                        });
                } else {
                    guildDetailItem.visible = false;
                }

                root.state = stateNormal;
            }).catch(function(reason) {
                setErrorText(reason.toString());
                root.state = stateNetworkError;
            });
    }
}
