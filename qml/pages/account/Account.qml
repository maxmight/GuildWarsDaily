import QtQuick 2.2
import Sailfish.Silica 1.0

import "../../components/" as Components
import "../../js/server.js" as Service

Components.NetworkResourcePage {
    id: root

    property string pageHeaderTitle
    property string pageHeaderDescription

    SilicaListView {
        id: listView
        anchors.fill: parent

        header: PageHeader {
            id: accountPageHeader
            title: pageHeaderTitle
            description: pageHeaderDescription
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Refresh")
                onClicked: loadContent()
            }
        }

        model: ListModel {
            //ListElement { scope: "progression"; enabled: false; text: qsTr("Achievements"); page: "Achievements.qml" }
            ListElement { scope: "characters"; enabled: false; text: qsTr("Characters"); page: "Characters.qml" }
            ListElement { scope: "unlocks"; enabled: false; text: qsTr("Dyes"); page: "Dyes.qml" }
            /*
            ListElement { scope: ""; enabled: false; text: "Gliders" }
            ListElement { scope: ""; enabled: false; text: "Masteries" }
            ListElement { scope: ""; enabled: false; text: "Materials" }
            ListElement { scope: ""; enabled: false; text: "Miniatures" }
            ListElement { scope: ""; enabled: false; text: "Outfits" }
            ListElement { scope: ""; enabled: false; text: "Raids" }
            ListElement { scope: ""; enabled: false; text: "Recipes" }
            */
            ListElement { scope: "progression"; enabled: false; text: qsTr("Mastery Points"); page: "MasteryPoints.qml" }
            ListElement { scope: "unlocks"; enabled: false; text: qsTr("Titles"); page: "Titles.qml" }
            ListElement { scope: "wallet"; enabled: false; text: qsTr("Wallet"); page: "Wallet.qml" }
        }

        delegate: ListItem {
            width: parent.width
            visible: root.state === stateNormal
            contentHeight: Theme.itemSizeSmall
            enabled: model.enabled
            onClicked: pageStack.push(Qt.resolvedUrl(model.page))

            Label {
                text: model.text
                color: model.enabled ? Theme.primaryColor : Theme.rgba(Theme.secondaryColor, 0.4)
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: Theme.horizontalPageMargin
                anchors.right: image.left
                anchors.rightMargin: Theme.paddingMedium
            }

            Image {
                id: image
                source: "image://theme/icon-m-right"
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                anchors.rightMargin: Theme.paddingLarge
            }
        }

        Component.onCompleted: {
            loadContent();
        }

        function refreshMenuItems(permissions) {
            for (var i = 0; i < model.count; i++) {
                var listElementItem = model.get(i);
                listElementItem.enabled =
                        permissions.indexOf(listElementItem.scope) !== -1;
            }
        }
    }

    function loadContent() {
        root.state = stateLoading;

        Service.getAccountDetails(accountSettings.accessToken)
            .then(function(result) {
                root.state = stateNormal;

                pageHeaderTitle = result.name;
                pageHeaderDescription = result.world;
                listView.refreshMenuItems(result.permissions);
            }).catch(function(reason) {
                setErrorText(reason.toString());
                root.state = stateNetworkError;
            });
    }
}
