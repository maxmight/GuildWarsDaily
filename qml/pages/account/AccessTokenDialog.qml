import QtQuick 2.2
import Sailfish.Silica 1.0

Dialog {
    id: root

    property bool required: true

    Column {
        width: parent.width
        spacing: Theme.paddingLarge

        DialogHeader {}

        TextField {
            id: accessTokenTextField
            width: parent.width
            placeholderText: qsTr("Access Token")
            label: qsTr("Access Token")

            EnterKey.iconSource: "image://theme/icon-m-enter-accept"
            EnterKey.onClicked: root.accept()
        }

        Label {
            x: Theme.horizontalPageMargin
            width: parent.width - 2 * x
            wrapMode: Text.Wrap
            text: qsTr("API keys are a way to grant third-party tools and applications partial read-only access to your Guild Wars 2 account.")
        }

        Label {
            x: Theme.horizontalPageMargin
            width: parent.width - (2 * x)
            wrapMode: Text.Wrap
            text: qsTr("Each key is granted a fixed set of permissions that limit what third-party tools can access, and you can delete a key at any time to revoke the permissions granted.")
        }

        LinkedLabel {
            x: Theme.horizontalPageMargin
            width: parent.width - 2 * x
            color: Theme.primaryColor
            linkColor: Theme.highlightColor
            shortenUrl: true
            plainText: qsTr("API keys are created at http://account.arena.net/applications.")
        }
    }

    canAccept: required ? accessTokenTextField.text.length > 0 : true

    onOpened: {
        accessTokenTextField.text = accountSettings.accessToken
    }

    onAccepted: {
        accountSettings.accessToken = accessTokenTextField.text
        accountSettings.sync();

        acceptDestination = Qt.resolvedUrl("Account.qml");
        acceptDestinationAction = PageStackAction.Replace;
    }
}
