import QtQuick 2.2
import Sailfish.Silica 1.0

import "../js/events.js" as Events

Page {
    property int currentTimeFrameIndex: -1
    property var currentEventDeadlines: {

    }

    PageHeader {
        id: pageHeader
        title: qsTr("Event Timers")
    }

    Column {
        id: currentEvents
        width: parent.width

        anchors {
            top: pageHeader.bottom
        }

        Repeater {
            id: currentEventsRepeater
            width: parent.width

            model: ListModel {
                id: currentEventsModel
            }

            Row {
                width: parent.width

                Rectangle {
                    width: 10 * Theme.pixelRatio
                    height: parent.height
                    color: getDifficultyColor(model.difficulty)
                }

                Label {
                    text: model.remainingTime
                    width: parent.width / 5
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: text.split(":")[1] === "00" ? Theme.highlightColor : Theme.primaryColor

                    Behavior on color {
                        ColorAnimation { duration: 200 }
                    }
                }

                Column {
                    Label {
                        text: model.name
                        font.pixelSize: Theme.fontSizeSmall
                        color: Theme.highlightColor
                    }

                    Label {
                        height: contentHeight + Theme.paddingSmall
                        text: model.event
                        font.pixelSize: Theme.fontSizeExtraSmall
                        color: Theme.secondaryHighlightColor
                    }
                }
            }
        }

        Row {
            width: parent.width

            Separator {
                width: parent.width / 2
                color: Theme.secondaryHighlightColor
                rotation: 180
            }

            Separator {
                width: parent.width / 2
                color: Theme.secondaryHighlightColor
            }
        }
    }

    SilicaListView {
        width: parent.width
        clip: true

        anchors {
            top: currentEvents.bottom
            bottom: parent.bottom
        }

        model: ListModel {
            id: upcomingEventsModel
        }

        delegate: ListItem {
            id: upcomingEventListItem
            width: parent.width
            contentHeight: Theme.itemSizeSmall

            Row {
                width: parent.width
                height: parent.height

                Rectangle {
                    width: 10 * Theme.pixelRatio
                    height: parent.height
                    color: getDifficultyColor(model.difficulty)
                }

                Label {
                    text: model.time
                    width: parent.width / 5
                    anchors.verticalCenter: parent.verticalCenter
                    horizontalAlignment: Text.AlignHCenter
                    color: upcomingEventListItem.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                }

                Label {
                    text: model.name
                    font.pixelSize: Theme.fontSizeSmall
                    anchors.verticalCenter: parent.verticalCenter
                    color: upcomingEventListItem.highlighted ? Theme.highlightColor : Theme.primaryColor
                }
            }
        }

        VerticalScrollDecorator {}
    }

    Timer {
        id: eventTimer
        interval: 1000
        repeat: true

        onTriggered: {
            updateCurrentEvents();
        }
    }

    Component.onCompleted: {
        updateCurrentEvents();
        eventTimer.running = true;
    }

    function updateCurrentEvents() {
        var now = new Date();
        var index = getItemIndex(now.getUTCHours(), now.getUTCMinutes());

        if (currentTimeFrameIndex !== index) {
            currentEventDeadlines = {};
            currentTimeFrameIndex = index;
            currentEventsModel.clear();

            var newEvents = Events.events[index];

            newEvents.forEach(function(eventId) {
                currentEventDeadlines[eventId] = calculateDeadlineForIndex(index);

                var event = {
                    id: eventId,
                    name: Events.eventTypes[eventId].name,
                    event: Events.eventTypes[eventId].event,
                    difficulty: Events.eventTypes[eventId].difficulty,
                    remainingTime: calculateRemainingTime(eventId),
                };

                currentEventsModel.append(event);
            });

            updateUpcomingEvents(index);
        } else {
            for (var i = 0; i < currentEventsModel.count; i++) {
                var event = currentEventsModel.get(i);
                currentEventsModel.get(i).remainingTime = calculateRemainingTime(event.id);
            }
        }
    }

    function updateUpcomingEvents(index) {
        upcomingEventsModel.clear();

        for (var i = 0; i < 96; i++) {
            index = incrementIndex(index);

            var events = Events.events[index];

            events.forEach(function(eventId) {
                var event = {
                    name: Events.eventTypes[eventId].name,
                    difficulty: Events.eventTypes[eventId].difficulty,
                    time: calculateStartTimeForIndex(index)
                };

                upcomingEventsModel.append(event);
            });
        }
    }

    function calculateStartTimeForIndex(index) {
        var hours = parseInt(index / 4)
        var minutes = (index % 4) * 15

        // Convert UTC to local time
        var now = new Date();
        now.setHours(hours);
        now.setMinutes(minutes + (now.getTimezoneOffset() * -1));

        hours = now.getHours();
        minutes = now.getMinutes();

        if (minutes < 10) {
            minutes = "0" + minutes;
        }

        return hours + ":" + minutes;
    }

    function calculateRemainingTime(eventId) {
        var deadline = currentEventDeadlines[eventId];

        var now = new Date();
        var difference = (deadline - now) / 1000;

        var minutes = parseInt(difference / 60);
        var seconds = parseInt(difference - (minutes * 60));
        if (seconds < 10) {
            seconds = "0" + seconds;
        }

        return minutes + ":" + seconds;
    }

    function calculateDeadlineForIndex(index) {
        var hours = parseInt(index / 4)
        var minutes = (index % 4) * 15

        var result = new Date()
        result.setMinutes(minutes)
        result.setMinutes(result.getMinutes() + 15)
        result.setSeconds(0);

        return result
    }

    function incrementIndex(index) {
        index += 1;

        if (index > 95) {
            index = 0;
        }

        return index;
    }

    function getItemIndex(hours, minutes) {
        var result = hours * 4

        if (minutes > 0) {
            if (minutes < 15) {
                result += 0
            } else if (minutes < 30) {
                result += 1
            } else if (minutes < 45) {
                result += 2
            } else {
                result = (hours === 23) ? 0 : result + 3
            }
        }

        return result
    }

    function getDifficultyColor(difficulty) {
        switch (difficulty) {
        case "easy":
        default:
            return "#80efe943";
        case "medium":
            return "#80ef9343";
        case "hard":
            return "#80ef4348";
        }
    }
}
