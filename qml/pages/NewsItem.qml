import QtQuick 2.2
import Sailfish.Silica 1.0

import "../components/" as Components

Page {
    property string newsItemTitle;
    property string newsItemContent;
    property string newsItemDate;
    property string newsItemCreator;

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {}

        Column {
            id: column

            width: parent.width
            height: childrenRect.height

            spacing: Theme.paddingMedium

            PageHeader {
                title: qsTr("News")
            }

            Label {
                width: parent.width - (2 * Theme.horizontalPageMargin)
                anchors.horizontalCenter: parent.horizontalCenter

                text: newsItemTitle
                font {
                    pixelSize: Theme.fontSizeMedium
                    family: Theme.fontFamilyHeading
                }
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: Theme.highlightColor
            }

            Label {
                width: parent.width - (2 * Theme.horizontalPageMargin)
                anchors.horizontalCenter: parent.horizontalCenter

                font.pixelSize: Theme.fontSizeExtraSmall
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: Theme.secondaryColor
                text: newsItemDate + " " + qsTr("by") + " " + newsItemCreator
            }

            Components.RescalingRichText {
                width: parent.width - (2 * Theme.horizontalPageMargin)
                anchors.horizontalCenter: parent.horizontalCenter

                color: Theme.primaryColor
                fontSize: Theme.fontSizeSmall
                text: newsItemContent.replace(new RegExp("src=\"//", "g"), "src=\"https://")

                onLinkActivated: {
                    Qt.openUrlExternally(link)
                }
            }
        }
    }
}
