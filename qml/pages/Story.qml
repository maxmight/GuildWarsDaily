import QtQuick 2.2
import Sailfish.Silica 1.0

import "../components/" as Components
import "../js/server.js" as Service

Components.NetworkResourcePage {
    property var stories: []
    property string chapterName

    id: root

    SilicaListView {
        id: listView
        anchors.fill: parent

        header: PageHeader {
            title: chapterName
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        }

        model: ListModel {
            id: listModel
        }

        VerticalScrollDecorator {}

        PullDownMenu {
            quickSelect: true

            MenuItem {
                text: qsTr("Refresh")
                onClicked: listView.loadContent()
            }
        }

        delegate: ListItem {
            id: listItem
            width: parent.width
            contentHeight: nameLabel.height + timelineLabel.height + descriptionLabel.height + (2 * Theme.paddingMedium)

            Label {
                id: nameLabel

                y: Theme.paddingMedium
                width: parent.width

                text: model.name
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: Theme.highlightColor

                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
            }

            Label {
                id: timelineLabel
                height: contentHeight

                text: model.timeline
                font.pixelSize: Theme.fontSizeExtraSmall
                color: Theme.secondaryColor

                anchors {
                    top: nameLabel.bottom
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.paddingLarge
                    rightMargin: Theme.paddingLarge
                }
            }

            Label {
                id: descriptionLabel
                height: text.length > 0 ? contentHeight : 0

                text: model.description
                font.pixelSize: Theme.fontSizeSmall
                color: Theme.primaryColor
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere

                anchors {
                    top: timelineLabel.bottom
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.paddingLarge
                    rightMargin: Theme.paddingLarge
                }
            }
        }

        Component.onCompleted: {
            loadContent();
        }

        function loadContent() {
            listModel.clear();
            root.state = stateLoading;

            Service.getStories(stories, function(result) {
                root.state = stateNormal;

                result.sort(function(a, b) {
                    return a.order - b.order;
                });

                listModel.append(result);
            }, function(statusCode, statusText) {
                setErrorMessage(statusCode, statusText);
                root.state = stateNetworkError;
            });
        }
    }
}
