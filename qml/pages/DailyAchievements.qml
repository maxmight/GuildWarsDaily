import QtQuick 2.2
import Sailfish.Silica 1.0

import "../components/" as Components
import "../js/server.js" as Service

Components.NetworkResourcePage {
    id: root

    property int day;

    readonly property int today: 1
    readonly property int tomorrow: 2

    SilicaListView {
        id: listView
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Daily Achievements")
            description: day == today ? qsTr("Today") : qsTr("Tomorrow")
        }

        model: ListModel {
            id: listModel
        }

        PullDownMenu {
            quickSelect: true

            MenuItem {
                text: qsTr("Refresh")
                onClicked: listView.loadContent()
            }
        }

        section {
            property: "type"
            criteria: ViewSection.FullString
            delegate: SectionHeader {
                text: section
            }
        }

        delegate: ListItem {
            id: listItem

            width: parent.width
            contentHeight: nameLabel.height + descriptionLabel.height + (2 * Theme.paddingMedium)

            anchors {
                left: parent.left
                right: parent.right
            }

            onClicked: {
                pageStack.push(Qt.resolvedUrl("ViewAchievement.qml"), {"achievement": listModel.get(index)})
            }

            Label {
                id: nameLabel

                y: Theme.paddingMedium
                width: parent.width

                text: name
                font.pixelSize: Theme.fontSizeMedium
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: listItem.highlighted ? Theme.highlightColor : Theme.primaryColor

                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
            }

            Label {
                id: descriptionLabel

                height: text.length > 0 ? contentHeight : 0

                text: description
                font.pixelSize: Theme.fontSizeSmall
                color: listItem.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere

                anchors {
                    top: nameLabel.bottom
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                    bottomMargin: Theme.paddingMedium
                }
            }
        }

        VerticalScrollDecorator {}

        Component.onCompleted: {
            loadContent();
        }

        function loadContent() {
            listModel.clear();
            root.state = stateLoading;

            Service.getDailyAchievements(day == today)
                .then(function(achievements) {
                    root.state = stateNormal;
                    listModel.append(achievements);
                }).catch(function(reason) {
                    setErrorText(reason.toString());
                    root.state = stateNetworkError;
                });
        }
    }

    onStatusChanged: {
        if (status == PageStatus.Active) {
            if (day == today) {
                pageStack.pushAttached(
                            Qt.resolvedUrl("DailyAchievements.qml"),
                            {"day": tomorrow});
            }
        }
    }
}
