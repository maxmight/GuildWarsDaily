import QtQuick 2.2
import Sailfish.Silica 1.0

import "../components/" as Components

Page {
    id: homePage

    SilicaListView {
        anchors.fill: parent

        header: PageHeader {
            title: qsTr("Guild Wars Daily")
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("About")
                onClicked: pageStack.push(Qt.resolvedUrl("About.qml"))
            }
        }

        model: VisualItemModel {
            Components.CommonListItem {
                titleText: qsTr("News")
                descriptionText: qsTr("Knowledge is the greatest treasure.")
                onClicked: pageStack.push(Qt.resolvedUrl("News.qml"));
            }

            Components.CommonListItem {
                id: accessTokenListItem
                titleText: qsTr("Account")
                descriptionText: qsTr("Prepare for adventure.")
                onClicked: {
                    if (accountSettings.accessToken === "") {
                        showAccessTokenDialog();
                    } else {
                        pageStack.push(Qt.resolvedUrl("account/Account.qml"));
                    }
                }

                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Set Access Token")
                        onClicked: showAccessTokenDialog()
                    }

                    MenuItem {
                        text: qsTr("Delete Access Token")
                        enabled: accountSettings.accessToken != ""

                        RemorseItem { id: remorse }

                        onClicked: {
                            remorse.execute(accessTokenListItem,
                                            qsTr("Deleting"),
                                            function() { accountSettings.accessToken = ""; });
                        }
                    }
                }
            }

            Components.CommonListItem {
                titleText: qsTr("Daily Achievements")
                descriptionText: qsTr("Ready for the next one.")
                onClicked: pageStack.push(Qt.resolvedUrl("DailyAchievements.qml"),
                                          {"day": 1});
            }

            Components.CommonListItem {
                titleText: qsTr("World vs. World")
                descriptionText: qsTr("We all must do our part.")
                onClicked: pageStack.push(Qt.resolvedUrl("WvW.qml"));
            }

            Components.CommonListItem {
                titleText: qsTr("Event Timers")
                descriptionText: qsTr("There's something in the water. Help!")
                onClicked: pageStack.push(Qt.resolvedUrl("EventTimers.qml"));
            }

            Components.CommonListItem {
                titleText: qsTr("Currency Exchange")
                descriptionText: qsTr("Looking to spend some coin?")
                onClicked: pageStack.push(Qt.resolvedUrl("CurrencyExchange.qml"));
            }

            Components.CommonListItem {
                titleText: qsTr("Storylines")
                descriptionText: qsTr("History never lies. Historians however...")
                onClicked: pageStack.push(Qt.resolvedUrl("Storylines.qml"));
            }
        }

        VerticalScrollDecorator {}
    }

    function showAccessTokenDialog() {
        var dialog = pageStack.push(Qt.resolvedUrl("account/AccessTokenDialog.qml"));
    }
}
