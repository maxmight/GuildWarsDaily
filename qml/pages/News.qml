import QtQuick 2.2
import QtQuick.XmlListModel 2.0
import Sailfish.Silica 1.0

import "../components/" as Components

Page {
    Components.TextPlaceholder {
        id: viewPlaceholder
        anchors.centerIn: parent
        visible: newsModel.status === XmlListModel.Error
        text: qsTr("Error Loading RSS Feed")
    }

    BusyIndicator {
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: newsModel.status === XmlListModel.Loading
    }

    SilicaListView {
        anchors.fill: parent
        visible: newsModel.status != XmlListModel.Loading

        header: PageHeader {
            title: qsTr("News")
        }

        VerticalScrollDecorator {}

        PullDownMenu {
            quickSelect: true

            MenuItem {
                text: qsTr("Refresh")
                onClicked: newsModel.reload()
            }
        }

        model: XmlListModel {
            id: newsModel
            source: "https://www.guildwars2.com/en/feed/"
            query: "/rss/channel/item"

            onStatusChanged: {
                if (status === XmlListModel.Error) {
                    viewPlaceholder.hintText = errorString();
                }
            }

            namespaceDeclarations: "declare namespace content = 'http://purl.org/rss/1.0/modules/content/';"
                                   + "declare namespace dc = 'http://purl.org/dc/elements/1.1/';"
            XmlRole { name: "title"; query: "title/string()" }
            XmlRole { name: "date"; query: "pubDate/string()" }
            XmlRole { name: "content"; query: "content:encoded/string()" }
            XmlRole { name: "creator"; query: "dc:creator/string()" }
        }

        delegate: ListItem {
            id: listItem
            width: parent.width
            contentHeight: titleLabel.height + dateLabel.height + (2 * Theme.paddingMedium)

            onClicked: {
                pageStack.push(
                            Qt.resolvedUrl("NewsItem.qml"),
                            {
                                "newsItemTitle": model.title,
                                "newsItemContent": model.content,
                                "newsItemDate": toLocalDate(model.date),
                                "newsItemCreator": model.creator
                            });
            }

            Label {
                id: titleLabel
                width: parent.width

                text: model.title
                font.pixelSize: Theme.fontSizeMedium
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: listItem.highlighted ? Theme.highlightColor : Theme.primaryColor

                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                    topMargin: Theme.paddingMedium
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
            }

            Label {
                id: dateLabel
                width: contentWidth
                text: toLocalDate(model.date)

                font.pixelSize: Theme.fontSizeSmall
                color: listItem.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor

                anchors {
                    top: titleLabel.bottom
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
            }
        }
    }

    function toLocalDate(pubDate) {
        var dateString = pubDate.replace(',', '').split(' ');

        if (dateString.length !== 6) {
            return pubDate;
        }

        var date = new Date([
                            dateString[0],
                            dateString[2],
                            dateString[1],
                            dateString[3],
                            dateString[4],
                            'GMT' + dateString[5]].join(' '));

        if (isNaN(date.getDate())) {
            return pubDate;
        }

        return date.toLocaleDateString();
    }
}
