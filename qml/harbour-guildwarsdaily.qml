import QtQuick 2.0
import Sailfish.Silica 1.0
import com.cutehacks.duperagent 1.0 as Http

import "pages"
import "settings" as Settings

ApplicationWindow {
    initialPage: Component { HomePage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: Orientation.All
    _defaultPageOrientations: Orientation.All

    Settings.AccountSettings {
        id: accountSettings
    }

    Component.onCompleted: {
        Http.Request.config({
            maxSize: 20 * 1024 * 1024
        });
    }
}
