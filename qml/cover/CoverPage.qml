import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    Image {
        y: parent.height / 3 - height / 2
        x: parent.width / 2 - width / 2
        source: "../images/logo.svg"
    }

    Label {
        id: label
        y: (parent.height / 3) * 2 - height / 2
        width: parent.width
        text: "Guild Wars\nDaily"
        font.pixelSize: Theme.fontSizeMedium
        horizontalAlignment: Text.AlignHCenter
    }
}
