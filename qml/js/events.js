var eventTypes = {
    1: {
        "name": "Great Jungle Wurm",
        "difficulty": "easy",
        "event": "The Battle for Wychmire Swamp",
        "level": "15"
    },
    2: {
        "name": "Shadow Behemoth",
        "difficulty": "easy",
        "event": "Secrets in the Swamp",
        "level": "15"
    },
    3: {
        "name": "The Frozen Maw",
        "difficulty": "easy",
        "event": "Svanir Shaman",
        "level": "10"
    },
    4: {
        "name": "Fire Elemental",
        "difficulty": "easy",
        "event": "Thaumanova Reactor Fallout",
        "level": "15"
    },
    5: {
        "name": "The Shatterer",
        "difficulty": "medium",
        "event": "Kralkatorrik's Legacy",
        "level": "50"
    },
    6: {
        "name": "Modniir Ulgoth",
        "difficulty": "medium",
        "event": "Seraph Assault on Centaur Camps",
        "level": "43"
    },
    7: {
        "name": "Golem Mark II",
        "difficulty": "medium",
        "event": "Inquest Golem Mark II",
        "level": "68"
    },
    8: {
        "name": "Claw of Jormag",
        "difficulty": "medium",
        "event": "Breaking the Claw of Jormag",
        "level": "80"
    },
    9: {
        "name": "Admiral Taidha Covington",
        "difficulty": "medium",
        "event": "The Campaign Against Taidha Covington",
        "level": "50"
    },
    10: {
        "name": "Megadestroyer",
        "difficulty": "medium",
        "event": "The Battle for Mount Maelstrom",
        "level": "66"
    },
    11: {
        "name": "Karka Queen",
        "difficulty": "hard",
        "event": "Fun in the Southsun",
        "level": "80"
    },
    12: {
        "name": "Tequatl the Sunless",
        "difficulty": "hard",
        "event": "Danger at Fabled Djannor",
        "level": "65"
    },
    13: {
        "name": "Evolved Jungle Wurm",
        "difficulty": "hard",
        "event": "Triple Trouble",
        "level": "55"
    }
};

// Events in UTC Time
var events = [
    [9, 12],    //    "0:00"
    [3],        //    "0:15"
    [10],       //    "0:30"
    [4],        //    "0:45"
    [5, 13],    //    "1:00"
    [1],        //    "1:15"
    [6],        //    "1:30"
    [2],        //    "1:45"
    [7, 11],    //    "2:00"
    [3],        //    "2:15"
    [8],        //    "2:30"
    [4],        //    "2:45"
    [9, 12],    //    "3:00"
    [1],        //    "3:15"
    [10],       //    "3:30"
    [2],        //    "3:45"
    [5, 13],    //    "4:00"
    [3],        //    "4:15"
    [6],        //    "4:30"
    [4],        //    "4:45"
    [7],        //    "5:00"
    [1],        //    "5:15"
    [8],        //    "5:30"
    [2],        //    "5:45"
    [9, 11],    //    "6:00"
    [3],        //    "6:15"
    [10],       //    "6:30"
    [4],        //    "6:45"
    [5, 12],    //    "7:00"
    [1],        //    "7:15"
    [6],        //    "7:30"
    [2],        //    "7:45"
    [7,13],     //    "8:00"
    [3],        //    "8:15"
    [8],        //    "8:30"
    [4],        //    "8:45"
    [9],        //    "9:00"
    [1],        //    "9:15"
    [10],       //    "9:30"
    [2],        //    "9:45"
    [5],        //    "10:00"
    [3],        //    "10:15"
    [6, 11],    //    "10:30"
    [4],        //    "10:45"
    [7],        //    "11:00"
    [1],        //    "11:15"
    [8, 12],    //    "11:30"
    [2],        //    "11:45"
    [9],        //    "12:00"
    [3],        //    "12:15"
    [10, 13],   //    "12:30"
    [4],        //    "12:45"
    [5],        //    "13:00"
    [1],        //    "13:15"
    [6],        //    "13:30"
    [2],        //    "13:45"
    [7],        //    "14:00"
    [3],        //    "14:15"
    [8],        //    "14:30"
    [4],        //    "14:45"
    [9, 11],    //    "15:00"
    [1],        //    "15:15"
    [10],       //    "15:30"
    [2],        //    "15:45"
    [5, 12],    //    "16:00"
    [3],        //    "16:15"
    [6],        //    "16:30"
    [4],        //    "16:45"
    [7, 13],    //    "17:00"
    [1],        //    "17:15"
    [8],        //    "17:30"
    [2],        //    "17:45"
    [9, 11],    //    "18:00"
    [3],        //    "18:15"
    [10],       //    "18:30"
    [4],        //    "18:45"
    [5, 12],    //    "19:00"
    [1],        //    "19:15"
    [6],        //    "19:30"
    [2],        //    "19:45"
    [7, 13],    //    "20:00"
    [3],        //    "20:15"
    [8],        //    "20:30"
    [4],        //    "20:45"
    [9],        //    "21:00"
    [1],        //    "21:15"
    [10],       //    "21:30"
    [2],        //    "21:45"
    [5],        //    "22:00"
    [3],        //    "22:15"
    [6],        //    "22:30"
    [4],        //    "22:45"
    [7, 11],    //    "23:00"
    [1],        //    "23:15"
    [8],        //    "23:30"
    [2]         //    "23:45"
];
