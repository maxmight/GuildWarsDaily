.import com.cutehacks.duperagent 1.0 as Http
.import "utilities.js" as Utilities

function getServerAddress() {
    return "https://api.guildwars2.com";
};

// Middleware function for prepending the base URL
var prefix = function(request) {
  var prefix = "https://api.guildwars2.com";
  if (request.url[0] === '/') {
    request.url = prefix + request.url;
  }
  return request;
};

function request(method, url, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState !== XMLHttpRequest.DONE) {
            return;
        }

        if ((xhr.status >= 200 && xhr.status < 300) || xhr.status === 304) {
            success(JSON.parse(xhr.responseText.toString()));
        } else {
            error(xhr.status, xhr.statusText);
        }
    };

    xhr.open(method, url);
    xhr.send();
};

function getDailyAchievements(today) {
    var url = today ? "/v2/achievements/daily" : "/v2/achievements/daily/tomorrow";

    return Http.Request
        .get(url)
        .use(prefix)
        .cacheSave(false)
        .then(function(dailyAchievementsResponse) {
            var achievements = [];
            var achievementIdentifiers = [];

            dailyAchievementsResponse.body.pve.forEach(function(value, index) {
                value.type = 'PvE';
                achievements[value.id] = value;
                achievementIdentifiers.push(value.id);
            });

            dailyAchievementsResponse.body.pvp.forEach(function(value, index) {
                value.type = 'PvP';
                achievements[value.id] = value;
                achievementIdentifiers.push(value.id);
            });

            dailyAchievementsResponse.body.wvw.forEach(function(value, index) {
                value.type = 'WvW';
                achievements[value.id] = value;
                achievementIdentifiers.push(value.id);
            });

            dailyAchievementsResponse.body.fractals.forEach(function(value, index) {
                value.type = 'Fractals';
                achievements[value.id] = value;
                achievementIdentifiers.push(value.id);
            });

            dailyAchievementsResponse.body.special.forEach(function(value, index) {
                value.type = 'Special';
                achievements[value.id] = value;
                achievementIdentifiers.push(value.id);
            });

            return getAchievements(achievementIdentifiers.join(','))
                .then(function(achievementsResponse) {
                    var result = [];

                    achievementsResponse.body.forEach(function(achievement, index) {
                        var access = [];

                        achievements[achievement.id]["required_access"].forEach(function(value){
                            access.push({"name": value});
                        });

                        result.push({
                                        name: achievement.name,
                                        type: achievements[achievement.id].type,
                                        description: achievement.description,
                                        requirement: achievement.requirement,
                                        level: achievements[achievement.id].level,
                                        requiredAccess: access
                                    });
                        });
                        return Utilities.groupBy(result, "type").flat();
                });
        });
};

function getWorlds(ids) {
    return Http.Request
        .get("/v2/worlds")
        .use(prefix)
        .query({ids: ids})
        .cacheLoad(Http.CacheControl.PreferCache);
};

function getWorldMatches(world) {
    return Http.Request
        .get("/v2/wvw/matches")
        .use(prefix)
        .query({world: world})
        .cacheSave(false);
};

function exchangeCoins(quantity, success, error) {
    request("GET", "https://api.guildwars2.com/v2/commerce/exchange/coins?quantity=" + quantity, function(response) {
        success(response);
    }, error);
};

function exchangeGems(quantity, success, error) {
    request("GET", "https://api.guildwars2.com/v2/commerce/exchange/gems?quantity=" + quantity, function(response) {
        success(response);
    }, error);
};

function getStorySeasons(success, error) {
    request("GET", getServerAddress() + "/v2/stories/seasons", function(response) {
        request("GET", getServerAddress() + "/v2/stories/seasons?ids=" + response.join(","), function(storySeansResponse) {
            success(storySeansResponse);
        }, error)
    }, error);
};

function getStories(stories, success, error) {
    request("GET", getServerAddress() + "/v2/stories?ids=" + stories.join(","), function(response) {
        success(response);
    }, error);
};

function getAccount(accessToken) {
    return Http.Request
        .get("/v2/account")
        .use(prefix)
        .query({access_token: accessToken})
        .cacheSave(false)
        .then(function(accountResponse) {
            return getWorlds(accountResponse.body.world)
                .then(function(worldResponse) {
                    return {
                        name: accountResponse.body.name,
                        world: worldResponse.body[0].name
                    };
                });
        });
};

function getAccountPermissions(accessToken) {
    return Http.Request
        .get("/v2/tokeninfo")
        .use(prefix)
        .query({access_token: accessToken})
        .cacheSave(false)
        .then(function(permissionsResponse) {
            return permissionsResponse.body;
        });
};

function getAccountDetails(accessToken) {
    var p1 = getAccount(accessToken);
    var p2 = getAccountPermissions(accessToken);

    return Http.Promise.all([p1, p2])
        .then(function(values) {
            var result = {
                name: values[0].name,
                world: values[0].world,
                permissions: values[1].permissions
            };

            return result;
        });
};

function getAccountDyes(accessToken) {
    return Http.Request
        .get("/v2/account/dyes")
        .use(prefix)
        .query({access_token: accessToken})
        .cacheSave(false)
        .then(function(accountDyesResponse) {
            return getColors(accountDyesResponse.body);
        });
};

function getColors(ids) {
    return Http.Request
        .get("/v2/colors")
        .use(prefix)
        .query({ids: ids})
        .cacheLoad(Http.CacheControl.PreferCache)
        .then(function(colorsResponse) {
            return colorsResponse.body;
        });
};

function getAccountTitles(accessToken) {
    return Http.Request
        .get("/v2/account/titles")
        .use(prefix)
        .query({access_token: accessToken})
        .cacheSave(false)
        .then(function(accountTitlesResponse) {
            return getTitles(accountTitlesResponse.body);
        });
};

function getTitles(ids) {
    return Http.Request
        .get("/v2/titles")
        .use(prefix)
        .query({ids: ids})
        .cacheLoad(Http.CacheControl.PreferCache);
};

function getAccountWallet(accessToken) {
    return Http.Request
        .get("/v2/account/wallet")
        .use(prefix)
        .query({access_token: accessToken})
        .cacheSave(false)
        .then(function(walletResponse) {
            var ids = walletResponse.body.map(function(x) {
                return x.id;
            });

            return getCurrencies(ids, walletResponse.body);
        });
};

function getCurrencies(ids, currencyValues) {
    return Http.Request
        .get("/v2/currencies")
        .use(prefix)
        .query({ids: ids})
        .cacheLoad(Http.CacheControl.PreferCache)
        .then(function(currenciesResponse) {
            var currencies = currenciesResponse.body;

            currencies.forEach(function(currency, index) {
                for (var i = 0; i < currencyValues.length; i++) {
                    if (currencyValues[i].id === currency.id) {
                        currency.value = currencyValues[i].value;
                        break;
                    }
                }
            });

            return currencies;
        });
};

function getAccountAchievements(accessToken) {
    return Http.Request
        .get("/v2/account/achievements")
        .use(prefix)
        .query({access_token: accessToken})
        .cacheSave(false)
        .then(function(achievementsResponse) {
            var ids = achievementsResponse.body
                .filter(function(x) {
                    if (x.done) return false;

                    if (!x.hasOwnProperty("current")) return false;
                    if (!x.hasOwnProperty("max")) return false;

                    return true;
                })
                .map(function(x) {
                    return x.id;
                });

            return getAchievements(ids);
        });
};

function getAchievements(ids) {
    return Http.Request
        .get("/v2/achievements")
        .use(prefix)
        .query({ids: ids})
        .cacheLoad(Http.CacheControl.PreferCache)
        .then(function(achievementsResponse) {
            return Http.Promise.resolve(achievementsResponse);
        });
};

function getCharacters(accessToken) {
    return Http.Request
        .get("/v2/characters")
        .use(prefix)
        .query({access_token: accessToken})
        .cacheLoad(Http.CacheControl.PreferCache)
        .then(function(charactersResponse) {
            if (charactersResponse.body.length === 0) {
                return Http.Promise.reject("No characters");
            }

            return Http.Promise.resolve(charactersResponse.body);
        });
};

function getCharacterCore(accessToken, character) {
    return Http.Request
        .get("/v2/characters/" + character + "/core")
        .use(prefix)
        .query({access_token: accessToken})
        .cacheLoad(Http.CacheControl.PreferCache);
};

function getGuild(id) {
    return Http.Request
        .get("/v2/guild/" + id)
        .use(prefix)
        .cacheSave(false);
};

function getTitle(id) {
    return Http.Request
        .get("/v2/titles/" + id)
        .use(prefix)
        .cacheLoad(Http.CacheControl.PreferCache);
};

function getAccountMasteryPoints(accessToken) {
    return Http.Request
        .get("/v2/account/mastery/points")
        .use(prefix)
        .query({access_token: accessToken});
}
