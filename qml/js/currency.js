function quantityToCoins(quantity) {
    var gold = parseInt(quantity / 10000);
    var silver = parseInt((quantity - (gold * 10000)) / 100);
    var copper = parseInt(quantity - (gold * 10000) - (silver * 100));

    return { gold: gold, silver: silver, copper: copper };
}
