// Array.find() implementation for Qt 5.6 and older
if (!Array.prototype.find) {
  Array.prototype.find = function(predicate) {
    if (this === null) {
      throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
      throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;

    for (var i = 0; i < length; i++) {
      value = list[i];
      if (predicate.call(thisArg, value, i, list)) {
        return value;
      }
    }
    return undefined;
  };
}

if (!Array.prototype.flat) {
  Array.prototype.flat = function(depth) {
    if (depth === undefined) {
      depth = 1;
    }

    var flatten = function (arr, depth) {
      if (depth < 1) {
        return arr.slice();
      }

      return arr.reduce(function (acc, val) {
        return acc.concat(Array.isArray(val) ? flatten(val, depth - 1) : val);
      }, []);

    };

    return flatten(this, depth);
  };
}

// Groups objects in an array by property.
function groupBy(objectArray, property) {
  return objectArray.reduce(function (accumulator, currentValue) {
    var key = currentValue[property];

    var found = accumulator.find(function (subArray) {
        return subArray.some(function (element) {
        return element[property] === key;
      });
    });

    if (!found) {
        var newArray = [currentValue];
      accumulator.push(newArray);
    } else {
        found.push(currentValue);
    }

    return accumulator;
  }, []);
}
