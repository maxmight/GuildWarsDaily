import QtQuick 2.2
import Nemo.Configuration 1.0

ConfigurationGroup {
    path: "/apps/guildwarsdaily/account"

    property string accessToken: ""
}
