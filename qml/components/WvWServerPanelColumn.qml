import QtQuick 2.2
import Sailfish.Silica 1.0

Column {
    property alias name: nameLabel.text
    property alias value: valueLabel.text
    property alias fontsize: valueLabel.font.pixelSize

    Label {
        id: nameLabel

        width: parent.width

        horizontalAlignment: Text.AlignHCenter
        color: Theme.secondaryColor
        font {
            pixelSize: Theme.fontSizeExtraSmall
            capitalization: Font.SmallCaps
        }
    }

    Label {
        id: valueLabel

        width: parent.width

        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: Theme.fontSizeSmall
    }
}

