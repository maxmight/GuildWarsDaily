import QtQuick 2.2
import Sailfish.Silica 1.0

Page {
    readonly property string stateNormal: "normal"
    readonly property string stateLoading: "loading"
    readonly property string stateNetworkError: "networkerror"
    default property alias children: pageContainer.children

    state: stateNormal

    TextPlaceholder {
        id: viewPlaceholder
        width: parent.width - (2 * Theme.horizontalPageMargin)
        anchors.centerIn: parent
        visible: false
        text: qsTr("Connection Error")
        hintText: qsTr("Please check your internet connection")
    }

    BusyIndicator {
        id: busyIndicator
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: visible
    }

    Item {
        id: pageContainer
        anchors.fill: parent
    }

    states: [
        State {
            name: stateNormal
            PropertyChanges { target: viewPlaceholder; visible: false }
            PropertyChanges { target: busyIndicator; visible: false }
        },
        State {
            name: stateLoading
            PropertyChanges { target: viewPlaceholder; visible: false }
            PropertyChanges { target: busyIndicator; visible: true }
        },
        State {
            name: stateNetworkError
            PropertyChanges { target: viewPlaceholder; visible: true }
            PropertyChanges { target: busyIndicator; visible: false }
        }
    ]

    function setErrorMessage(statusCode, statusText) {
        if (statusCode !== 0 && statusText.length > 0) {
            viewPlaceholder.hintText = statusCode + " " + statusText;
        } else {
            viewPlaceholder.hintText = qsTr("Please check your internet connection");
        }
    }

    function setErrorText(errorText) {
        if (errorText.length > 0) {
            viewPlaceholder.hintText = errorText;
        } else {
            viewPlaceholder.hintText = qsTr("Please check your internet connection");
        }
    }
}
