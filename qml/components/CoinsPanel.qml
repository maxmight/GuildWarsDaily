import QtQuick 2.2
import Sailfish.Silica 1.0

Row {
    spacing: Theme.paddingMedium

    property alias goldCoinsValue: goldCoinsValueLabel.text
    property alias silverCoinsValue: silverCoinsValueLabel.text
    property alias copperCoinsValue: copperCoinsValueLabel.text

    Row {
        spacing: Theme.paddingSmall
        Label {
            id: goldCoinsValueLabel
        }
        Image {
            width: height
            height: goldCoinsValueLabel.height * 0.5
            anchors.verticalCenter: goldCoinsValueLabel.verticalCenter
            source: "../images/ui_coin_gold.svg"
        }
    }

    Row {
        spacing: Theme.paddingSmall
        Label {
            id: silverCoinsValueLabel
        }
        Image {
            width: height
            height: silverCoinsValueLabel.height * 0.5
            anchors.verticalCenter: silverCoinsValueLabel.verticalCenter
            source: "../images/ui_coin_silver.svg"
        }
    }

    Row {
        spacing: Theme.paddingSmall
        Label {
            id: copperCoinsValueLabel
        }
        Image {
            width: height
            height: copperCoinsValueLabel.height * 0.5
            anchors.verticalCenter: copperCoinsValueLabel.verticalCenter
            source: "../images/ui_coin_copper.svg"
        }
    }
}
