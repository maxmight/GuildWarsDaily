import QtQuick 2.2
import Sailfish.Silica 1.0

ListItem {
    id: root
    property alias titleText: title.text
    property alias descriptionText: description.text

    width: parent.width
    contentHeight: title.height + description.height + (2 * Theme.paddingMedium)

    Label {
        id: title
        y: Theme.paddingMedium

        font.pixelSize: Theme.fontSizeMedium
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        color: root.highlighted ? Theme.highlightColor : Theme.primaryColor

        anchors {
            left: parent.left
            right: parent.right
            leftMargin: Theme.horizontalPageMargin
            rightMargin: Theme.horizontalPageMargin
        }
    }

    Label {
        id: description
        height: text.length > 0 ? contentHeight : 0

        font.pixelSize: Theme.fontSizeSmall
        color: root.highlighted ? Theme.secondaryHighlightColor : Theme.secondaryColor
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere

        anchors {
            top: title.bottom
            left: parent.left
            right: parent.right
            leftMargin: Theme.horizontalPageMargin
            rightMargin: Theme.horizontalPageMargin
            bottomMargin: Theme.paddingMedium
        }
    }
}
