import QtQuick 2.2
import Sailfish.Silica 1.0

import "../components/" as Components

Column {
    id: panel

    property alias quantity: buyBlock.quantity
    property alias goldCoinsValue: buyBlock.goldCoinsValue
    property alias silverCoinsValue: buyBlock.silverCoinsValue
    property alias copperCoinsValue: buyBlock.copperCoinsValue
    property alias gemsValue: buyBlock.gemsValue

    Components.CurrencyExchangeBlock {
        id: buyBlock
        width: parent.width
        state: panel.state
    }
}
