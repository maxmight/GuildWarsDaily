import QtQuick 2.2
import Sailfish.Silica 1.0

import "../components/" as Components

Column {
    id: panel

    property alias quantity: textRowLabel2.text
    property alias goldCoinsValue: coinsPanel.goldCoinsValue
    property alias silverCoinsValue: coinsPanel.silverCoinsValue
    property alias copperCoinsValue: coinsPanel.copperCoinsValue
    property alias gemsValue: gemsValueLabel.text

    Item {
        width: parent.width
        height: textContainerRow.height

        Row {
            id: textContainerRow
            anchors.centerIn: parent
            width: textRowLabel1.width + textRowLabel2.width + textRowLabel3.width

            Label {
                id: textRowLabel1
                text: qsTr("You can exchange") + " "
            }
            Label {
                id: textRowLabel2
                color: Theme.highlightColor
            }
            Label {
                id: textRowLabel3
                text: " " + (panel.state == "GEMSTOCOINS" ? qsTr("gems for") : qsTr(
                                                         "gold coins for"))
            }
        }
    }

    Components.CoinsPanel {
        id: coinsPanel
        anchors.horizontalCenter: parent.horizontalCenter
    }

    Row {
        id: gemsPanel
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: Theme.paddingSmall

        Label {
            id: gemsValueLabel
        }
        Image {
            width: height
            height: gemsValueLabel.height * 0.6
            anchors.verticalCenter: gemsValueLabel.verticalCenter
            fillMode: Image.PreserveAspectFit
            source: "../images/ui_gem.svg"
        }
    }

    states: [
        State {
            name: "GEMSTOCOINS"
            PropertyChanges {
                target: coinsPanel
                visible: true
            }
            PropertyChanges {
                target: gemsPanel
                visible: false
            }
        },
        State {
            name: "COINSTOGEMS"
            PropertyChanges {
                target: coinsPanel
                visible: false
            }
            PropertyChanges {
                target: gemsPanel
                visible: true
            }
        }
    ]
}
