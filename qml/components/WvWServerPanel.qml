import QtQuick 2.2
import Sailfish.Silica 1.0

Column {
    width: parent.width

    property alias serverName: serverNameLabel.text
    property alias serverKills: serverKillsColumn.value
    property alias serverScore: serverScoreColumn.value
    property alias serverDeaths: serverDeathsColumn.value
    property alias serverCamps: campsDetailItem.value
    property alias serverTowers: towersDetailItem.value
    property alias serverKeeps: keepsDetailItem.value
    property alias serverCastle: castleDetailItem.value

    Label {
        id: serverNameLabel
        width: parent.width

        horizontalAlignment: Text.AlignHCenter
        font.pixelSize: Theme.fontSizeLarge
    }

    Row {
        width: parent.width

        Separator {
            width: parent.width / 2
            color: Theme.secondaryHighlightColor
            rotation: 180
        }

        Separator {
            width: parent.width / 2
            color: Theme.secondaryHighlightColor
        }
    }

    Row {
        width: parent.width

        WvWServerPanelColumn {
            id: serverKillsColumn
            width: parent.width / 3

            name: qsTr("kills")
        }

        WvWServerPanelColumn {
            id: serverScoreColumn
            width: parent.width / 3

            name: qsTr("score")
            fontsize: Theme.fontSizeMedium
        }

        WvWServerPanelColumn {
            id: serverDeathsColumn
            width: parent.width / 3

            name: qsTr("deaths")
        }
    }

    DetailItem {
        id: campsDetailItem
        label: qsTr("Camps")
    }

    DetailItem {
        id: towersDetailItem
        label: qsTr("Towers")
    }

    DetailItem {
        id: keepsDetailItem
        label: qsTr("Keeps")
    }

    DetailItem {
        id: castleDetailItem
        label: qsTr("Castle")
    }
}

