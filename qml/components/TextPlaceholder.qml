import QtQuick 2.2
import Sailfish.Silica 1.0

Item {
    property alias text: mainLabel.text
    property alias hintText: hintLabel.text

    height: mainLabel.height + (hintLabel.text.length > 0 ? hintLabel.height : 0)

    Text {
        id: mainLabel

        anchors {
            left: parent.left
            right: parent.right
        }

        font {
            pixelSize: Theme.fontSizeExtraLarge
            family: Theme.fontFamilyHeading
        }

        color: Theme.highlightColor
        opacity: 0.8
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.Wrap
    }

    Text {
        id: hintLabel

        anchors {
            left: parent.left
            right: parent.right
            top: mainLabel.bottom
        }

        font {
            pixelSize: Theme.fontSizeLarge
            family: Theme.fontFamilyHeading
        }

        color: Theme.highlightColor
        opacity: 0.5
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.Wrap
    }
}
